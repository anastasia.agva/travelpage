import React from 'react';
import ReactDOM from 'react-dom';
import './styles/common.scss';

import { App } from '@components/App/App';

const rootElement = document.getElementById('root');
ReactDOM.render(<App />, rootElement);
