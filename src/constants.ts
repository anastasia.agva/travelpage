export const pageTitles: Record<string, string> = {
  calendar: 'Календарь',
  feedback: 'Отзывы',
  shop: 'Магазин',
  contacts: 'Контакты',
};
