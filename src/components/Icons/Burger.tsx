import React, { FC } from 'react';

interface Props {
  className?: string;
}

export const Burger: FC<Props> = ({ className = '' }) => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={className}
    >
      <path d="M5 6H19" strokeWidth="2.5" strokeLinecap="round" />
      <path d="M5 12H19" strokeWidth="2.5" strokeLinecap="round" />
      <path d="M5 18H19" strokeWidth="2.5" strokeLinecap="round" />
    </svg>
  );
};
