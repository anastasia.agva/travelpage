import React, { FC } from 'react';
import { Header } from '@components/Header/Header';
import { Page } from '@components/Page/Page';
import './App.scss';

export const App: FC = () => {
  return (
    <div className="page">
      <Header />
      <main className="main">
        <div className="container">
          <Page />
        </div>k
      </main>
    </div>
  );
};
