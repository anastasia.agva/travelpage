import React, { FC } from 'react';
import logo from '@images/logo.svg';
import './Logo.scss';

export const Logo: FC = () => {
  return (
    <a href="/" className="logo">
      <img className="logo__image" src={logo} alt="Логотип" />
    </a>
  );
};
