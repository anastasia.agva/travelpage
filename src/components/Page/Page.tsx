import React, { FC } from 'react';
import './Page.scss';

export const Page: FC = () => {
  return (
    <>
      <h2 className="title">
        Путешествовать <span className="title__line">— круто!</span>
      </h2>
      <button className="button">Поехали с нами</button>
    </>
  );
};
