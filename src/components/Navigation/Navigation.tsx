import React, { FC } from 'react';
import classNames from 'classnames';
import './Navigation.scss';
import { pageTitles } from '../../constants';

interface Props {
  className?: string;
}

interface NavigationItemProps {
  title?: string;
  name?: string;
}

const NavigationItem: FC<NavigationItemProps> = ({ title, name = '' }) => {
  return (
    <li className="navigation__item" key={name}>
      <a href={`/${name}`} className="navigation__link">
        {title}
      </a>
    </li>
  );
};

export const Navigation: FC<Props> = ({ className = '' }) => {
  const arrayPageTitles = Object.entries(pageTitles);
  return (
    <nav className={classNames('navigation', className)}>
      <ul className="navigation__list">
        {arrayPageTitles.map(([name, title]) => {
          return <NavigationItem key={name} name={name} title={title} />;
        })}
      </ul>
    </nav>
  );
};
