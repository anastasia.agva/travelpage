import React, { FC, useEffect, useState, useRef } from 'react';
import { Navigation } from '@components/Navigation/Navigation';
import { CSSTransition } from 'react-transition-group';
import { Burger } from '@components/Icons/Burger';
import { Cross } from '@components/Icons/Cross';
import { Logo } from '@components/Logo/Logo';

export const MobileHeader: FC = () => {
  const [isOpenMenu, toggleMenu] = useState(false);
  const wrapperRef = useRef<HTMLDivElement>(null);

  const handleBtnClick = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.stopPropagation();
    toggleMenu(!isOpenMenu);
  };

  useEffect(() => {
    const documentClickListener = (e: MouseEvent) => {
      if (wrapperRef.current && !wrapperRef.current.contains(e.target as Node)) {
        toggleMenu(false);
      }
    };

    if (isOpenMenu) {
      console.log(isOpenMenu);
      document.addEventListener('click', documentClickListener as EventListener);
    } else {
      document.removeEventListener('click', documentClickListener as EventListener);
    }

    return () => {
      document.removeEventListener('click', documentClickListener as EventListener);
    };
  }, [isOpenMenu, wrapperRef]);

  return (
    <header className="header">
      <div className="container header__mobile-container">
        <Logo />
        <button className="header__mobile-button" onClick={handleBtnClick}>
          {isOpenMenu ? <Cross className="header__mobile-icon" /> : <Burger className="header__mobile-icon" />}
        </button>
      </div>
      <CSSTransition in={isOpenMenu} mountOnEnter unmountOnExit timeout={300} classNames="header-mobile-menu-animation">
        <div className="header__mobile-overlay" ref={wrapperRef}>
          <div className="header__mobile-menu">
            <Navigation className="header__mobile-navigation" />
          </div>
        </div>
      </CSSTransition>
    </header>
  );
};
