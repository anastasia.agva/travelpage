import React, { FC } from 'react';
import { Navigation } from '@components/Navigation/Navigation';
import { Logo } from '@components/Logo/Logo';

export const DesktopHeader: FC = () => {
  return (
    <header className="header">
      <div className="container header__container">
        <Logo />
        <Navigation className="header__navigation" />
      </div>
    </header>
  );
};
